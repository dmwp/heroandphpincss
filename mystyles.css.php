<?php
header( "Content-type: text/css; charset: UTF-8" );
global $lts;
	
$bgpage = get_field('site_background', 'option');

$tester = get_field('test', 'option');

?>

body{
	background-color:<?php echo ($bgpage) ?>;
}


.tester{
    background-color:<?php echo ($tester) ?>;
}



/* New Styles for container and hero */
.container{
    width:1200px;
}

.content{
    width:100%;
    margin:0 auto;
    padding:20px;
}
.content P{
    text-align: justify;
    font-size: 16px;
    line-height: 1.2;
    font-family: verdana;
    word-spacing:-2px;
    //word-break: break-all;    
}

.hero{
    height: 100vh;
    background-image:linear-gradient(rgba(0,0,0,0.7),rgba(0,0,0,0.7)), url('https://www.economist.com/sites/default/files/images/2015/09/blogs/economist-explains/code2.png');
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
}
.hero-content{
    color:#fff;
    padding:1em;
    font-family: verdana;
    text-align: center;
}
.hero-heading{
   margin:.5em 0;
    font-size:50px
}
.hero-subheading{
    margin: .5em 0;
    font-size: 20px;
}
.hero-button button{
    background: none;
    color:#fff;
    border: none;
    margin: 1em 0;
    padding:1em;
    border:solid 1px #fff;
    border-radius:5px;
}
}